package com.flycms.core.utils;

import com.flycms.constant.Const;
import com.flycms.module.user.model.User;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * Open source house, All rights reserved
 * 开发公司：28844.com<br/>
 * 版权：开源中国<br/>
 * <p>
 * 
 * 用户登录信息操作
 * 
 * <p>
 * 
 * 区分　责任人　日期　　　　说明<br/>
 * 创建　孙开飞　2017年5月25日 　<br/>
 * <p>
 * *******
 * <p>
 * 
 * @author sun-kaifei
 * @email admin@97560.com
 * @version 1.0 <br/>
 * 
 */
public class UserSessionUtils {

    /**
     * 读取用户SESSION信息
     * 
     * @param request
     * @return
     */
    public static User getLoginMember(HttpServletRequest request){
    	User loginMember = (User) request.getSession().getAttribute(Const.SESSION_USER);
        return loginMember;
    }

    /**
     * 写入用户SESSION信息
     * 
     * @param request
     * @param user
     */
    public static void setLoginMember(HttpServletRequest request,User user){
        request.getSession().setAttribute(Const.SESSION_USER,user);
    }

    /**
     * 用户访问链接转跳判断
     * 
     * @param request
     * @param redirectUrl
     * @return
     */
    public static String judgeLoginJump(HttpServletRequest request,String redirectUrl){
    	User user = getLoginMember(request);
    	if(user == null){
            String redirect = "redirect:/ucenter/login";
            if(!StringUtils.isEmpty(redirectUrl)){
                redirect += "?redirectUrl="+request.getContextPath() + redirectUrl;
            }
            return redirect;
        }
        return null;
    }
}
