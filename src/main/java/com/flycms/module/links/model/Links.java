/*
 *	Copyright © 2015 Zhejiang SKT Science Technology Development Co., Ltd. All rights reserved.
 *	浙江斯凯特科技发展有限公司 版权所有
 *	http://www.28844.com
 */

package com.flycms.module.links.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

/**
 * 友情链接实体
 * 
 * @author sunkaifei
 * 
 */
@Setter
@Getter
public class Links implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private int type;
	@NotEmpty(message="请填写网站名称！")
	private String linkName;
	@NotEmpty(message="请填写网站网址！")
	private String linkUrl;
	private String linkLogo;
	private int isShow;
	private int sort;
	//创建日期
	private Date createTime;
}
