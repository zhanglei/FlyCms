package com.flycms.web.tags;

import com.flycms.core.base.AbstractTagPlugin;
import com.flycms.module.user.service.UserService;
import freemarker.core.Environment;
import freemarker.template.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Open source house, All rights reserved
 * 开发公司：28844.com<br/>
 * 版权：开源中国<br/>
 *
 * 查询是否已关注或者是该用户粉丝标签
 * 
 * @author sunkaifei
 * 
 */
@Service
public class Checkfollow extends AbstractTagPlugin {

	@Autowired
	protected UserService userService;

	@Override
	@SuppressWarnings("rawtypes")
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		DefaultObjectWrapperBuilder builder = new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25);
		try {
			// 获取页面的参数
			Integer userFollow = 0;
			// 获取文件的分页
			//审核设置，默认0
			Integer userFans = 0;
			//处理标签变量
			@SuppressWarnings("unchecked")
			Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(params);
			for(String str:paramWrap.keySet()){ 
				if("userFollow".equals(str)){
					userFollow = Integer.parseInt(paramWrap.get(str).toString());
				}
				if("userFans".equals(str)){
					userFans = Integer.parseInt(paramWrap.get(str).toString());
				}

			}
			if(userFollow==null && userFans==null){
				env.setVariable("result", builder.build().wrap(false));
				body.render(env.getOut());
			}else{
				boolean result = userService.checkUserFans(userFollow,userFans);
				env.setVariable("result", builder.build().wrap(result));
				body.render(env.getOut());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
